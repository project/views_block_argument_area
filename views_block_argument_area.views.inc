<?php

/**
 * @file
 * Provide views data for views_block_area module.
 */

/**
 * Implements hook_views_data().
 */
function views_block_argument_area_views_data() {
  $data = [];

  $data['views']['views_block_argument_area'] = [
    'title' => t('Block argument area'),
    'help' => t('Insert a block with argument inside an area.'),
    'area' => [
      'id' => 'views_block_argument_area',
    ],
  ];

  return $data;
}
